<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Gustavo',
            'email' => 'test@test.com',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('test'),
            'remember_token' => Str::random(26),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('users')->insert([
            'name' => 'Gustavo',
            'email' => 'test1@test.com',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('test1'),
            'remember_token' => Str::random(26),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
