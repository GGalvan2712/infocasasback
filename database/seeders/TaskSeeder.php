<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert([
            'name' => 'Curso Node',
            'completed' => false,
            'user_id' => 1,
            'completed_at' => null,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('tasks')->insert([
            'name' => 'Curso Express',
            'completed' => false,
            'user_id' => 1,
            'completed_at' => null,
            'created_at' => Carbon::yesterday(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('tasks')->insert([
            'name' => 'Prueba Tecnica InfoCasas',
            'completed' => true,
            'user_id' => 1,
            'completed_at' => Carbon::now()->toDate(),
            'created_at' => Carbon::yesterday(),
            'updated_at' => Carbon::now()
        ]);
    }
}
