<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $table = 'tasks';
    
    protected $fillable = ['name' , 'completed' , 'user_id' , 'completed_at'];
    
    static $rules = [
        'name' => 'required',
    ];
    
    static $messages = [
        'name.required' => 'El campo nombre es requerido.',
    ];
    
    protected static function booted()
    {
        static::creating(function ($task) {
            $task->user_id = auth('sanctum')->user()->id;
        });
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

}
