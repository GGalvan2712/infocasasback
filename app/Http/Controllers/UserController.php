<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function __construct(Request $request)
    {
        $this->model =  'App\Models\User';
        $this->request = $request;    
    }

    public function login(){

        $data = array();

        $validator = Validator::make($this->request->all() , $this->model::$loginRules , $this->model::$loginMessages);

        if(!$validator->fails()){

            $email = $this->request->input('email');
            $password = $this->request->input('password');

            $user = User::where('email' , $email)->first();

            if(Auth::attempt(['email' => $email, 'password' => $password])){
               
                $token = $this->request->user()->createToken('token_name');
                
                $data = [
                    'type' => 'Bearer',
                    'access_token' => $token->plainTextToken   
                ];

                return response()->default($data);

            }else{
                
               return response()->default("Credenciales invalidas" , false);   
            
            }

        }
            
        return response()->default($validator->errors() , false);

    }

    public function logout(){

        $user = $this->request->user();

        $user->currentAccessToken()->delete();

        return response()->default("Usted se deslogeo." , false);
    }
}
