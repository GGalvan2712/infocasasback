<?php

namespace App\Http\Controllers;


use App\Models\Task;
use App\Traits\ResourceTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller
{   
    use ResourceTrait;

    public function __construct(Request $request)
    {
        $this->user_id = auth('sanctum')->user() ?  auth('sanctum')->user()->id : 6;
        $this->model =  'App\Models\Task';
        $this->request = $request;
    }

    public function store(Request $request){

        $validator = Validator::make($request->all() , Task::$rules , Task::$messages);
        
        if(!$validator->fails()){

            $getTaskIncomplete = Task::where([
                ['user_id' , $this->user_id],
                ['name' , $request->input('name')],
                ['completed' , false]
            ])->first();
            
            if(!$getTaskIncomplete){

                $newTask = Task::create($request->all());
    
               return response()->default($newTask);
            }

            return response()->default("Ya existe una tarea sin completar con ese nombre." , false);
        }

        return response()->default($validator->errors() , false);
    }

   
    public function updateStatus(Request $request , $id){
       
        $getTask = Task::where([
            ['user_id' , $this->user_id],
            ['id' , $id],
            ['completed' , $request->input('completed')]
        ])->first();
        
        if($getTask){
            $getTask->completed = !$request->input('completed');
            $getTask->completed_at = !$request->input('completed') == true ?  Carbon::now() : null;

            if($getTask->save()){
                
               return response()->default($getTask);
            
            }
        }

        return response()->default("Objeto no encontrado" , false);
    }

}
