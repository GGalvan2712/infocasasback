<?php

namespace App\Traits;

use Exception;
use Illuminate\Support\Facades\Validator;

trait ResourceTrait
{
    public function index(){
        
        $search = $this->request->input('search');

        $model = $this->model::where('user_id' , $this->user_id);

        if($search != 'null'){
            $model->where('name' , 'like' , '%'.$search.'%');
        }

        $model = $model->get();
       
        return response()->default($model);
    }

    public function show($id){
            
        $model = $this->model::where([
            ['user_id' , $this->user_id],
            ['id' , $id]
        ])->first();

        return response()->default($model);
    }

    public function store(){

        $validator = Validator::make($this->request->all() , $this->model::$rules , $this->model::$messages);
        
        if(!$validator->fails()){

           $newTask = $this->model::create($this->request->all());
    
            return response()->default($newTask);
        
        }

        return response()->default($validator->errors() , false);
    }

    public function update($id){

        $validator = Validator::make($this->request->all() , $this->model::$rules , $this->model::$messages);

        if(!$validator->fails()){

           $getModel = $this->model::where([
               ['user_id' , $this->user_id],
               ['id' , $id]
           ])->first();

           if($getModel){

                try{

                    $getModel->update($this->request->all());
                
                }catch(Exception $ex){
                    return response()->default("No se pudo actualizar el objeto" , false);   
                }
                
                return response()->default($getModel->refresh());

            }

           return response()->default('No se pudo actualizar el objeto' , false);

        }

        return response()->default($validator->errors() , false);
    }

    public function destroy($id){

        $model = $this->model::where([
            ['user_id' , $this->user_id],
            ['id' , $id]
        ])->first();

        if($model){
            
            $model->delete();
                
            return response()->default('Objeto eliminado correctamente');
    
        }
        
        return response()->default('Objeto no encontrado' , false);
    }

}
