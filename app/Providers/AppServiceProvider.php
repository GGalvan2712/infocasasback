<?php

namespace App\Providers;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      Response::macro('default' , function ($data , $success = true){
        return response()->json([
            'success' => $data == null ? false : $success,
            'data' => $data == null ? 'Objeto no encontrado' : $data
        ]);
      });

    }
}
