<?php

use App\Http\Controllers\TaskController;
use App\Http\Controllers\UserController;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login' , [UserController::class , 'login']);

Route::middleware('auth:sanctum')->group(function(){
    
    Route::resource('tasks', TaskController::class);
    Route::post('/updateStatus/{id}' , [TaskController::class , 'updateStatus']);
    Route::post('/logout' , [UserController::class , 'logout']);

});